const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
let loadCacMau = () => {
  let chuoiMau = document.getElementById("colorContainer");
  for (let index = 0; index < colorList.length; index++) {
    if (0 === index) {
      chuoiMau.innerHTML += `<button class = "color-button ${colorList[index]} active"></button>`;
    } else {
      chuoiMau.innerHTML += `<button class = "color-button ${colorList[index]} "></button>`;
    }
  }
};
loadCacMau();
let chonMaMau = document.querySelectorAll(".color-button");
let home = document.getElementById("house");
thayDoiMau = (arr, vitri) => {
  for (let index = 0; index < chonMaMau.length; index++) {
    chonMaMau[index].classList.remove("active");
    chonMaMau[vitri].classList.add("active");
    home.className = "house " + arr;
  }
};
for (let index = 0; index < chonMaMau.length; index++) {
  chonMaMau[index].addEventListener("click", function () {
    thayDoiMau(colorList[index], index);
  });
}
