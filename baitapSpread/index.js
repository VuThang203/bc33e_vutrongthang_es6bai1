const textHD = document.querySelector(".heading");
let jump = (text) => {
  return [...text].map((item) => `<span>${item}</span>`).join("");
};
textHD.innerHTML = jump(textHD.innerText);
