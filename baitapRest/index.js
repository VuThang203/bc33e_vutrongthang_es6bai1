let tinhDTB = (...diem) => {
  let diemTB = 0;
  let ketQua = 0;
  return diem.forEach((item) => {
    diemTB += parseFloat(item);
    ketQua = (diemTB / diem.length).toFixed(2);
    document.getElementById("tbKhoi1").innerHTML = ketQua;
  });
};
document.getElementById("btnKhoi1").onclick = () => {
  let diemToan = document.getElementById("inpToan").value * 1;
  let diemLy = document.getElementById("inpLy").value * 1;
  let diemHoa = document.getElementById("inpHoa").value * 1;
  tinhDTB(diemToan, diemLy, diemHoa);
};
document.getElementById("btnKhoi2").onclick = () => {
  let diemVan = document.getElementById("inpVan").value * 1;
  let diemSu = document.getElementById("inpSu").value * 1;
  let diemDia = document.getElementById("inpDia").value * 1;
  let diemAnh = document.getElementById("inpEnglish").value * 1;
  tinhDTB(diemVan, diemSu, diemDia, diemAnh);
};
